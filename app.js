// 自动打包工具 0.1
// By BT 19.01.07
// Last Update 01.07
const pack = require('jr-pack');
const path = require('path');
require('bt-utils');

// 监听的主目录
global.listenDirectory = './app/';
// 监听的模板路径
global.templateDirectory = './app/teamplate/';
// 公共模板目录
global.commonDirectory = './app/teamplate/common/';
// 打包输出目录
global.outDirectory = '/dist/';
// 资源目录变量
global.resDirectoryDim = './assets/';
// 是否格式化代码
global.formatHtml = false;
// 是否单文件改变打包
global.singleListen = false;
// 是否包括公共组件
global.hasCommon = true;
// 是否开启px转Em模式
global.hasLessPx2Em = false;

/************** less配置 *****************/
// less输入目录，相对于项目根目录
global.lessDirectory = 'app/less';
// css输出目录，相对于项目根目录
global.cssDirectory = 'dist/assets/css';
// 是否开启px转Em模式
global.hasLessPx2Em = false;
// 忽略指定文件夹
global.ignore = []



pack.run();