const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const merge = require('webpack-merge');
const webpackBaseConfig = require('./webpack.base.config.js');
const { resolve } = require('path');
const path = require('path');
const fs = require("fs");
const _html = ['testing','plireport','homereport'];
const minify = require('html-minifier').minify;
require('bt-utils');

module.exports = merge(webpackBaseConfig, {
    output: {
        path: resolve(__dirname, './dist/assets/js/'),
        filename: '[name].bundle.js',
    }
});