const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {
        'index': './app/src/index.js'
    },
    module: {
        rules: [
            {test: /\.js$/,loader: 'babel-loader',exclude: /node_modules/},
            {test:/\.less$/,loader: "style-loader!css-loader!less-loader"},
            {test: /\.css$/,use: ExtractTextPlugin.extract({fallback: "style-loader",use: "css-loader"})},
            {test: /\.vue$/,loader: 'vue-loader',options: {loaders: {less: ExtractTextPlugin.extract({use: ['css-loader?minimize', 'autoprefixer-loader', 'less-loader'],fallback: 'vue-style-loader'}),css: ExtractTextPlugin.extract({use: ['css-loader', 'autoprefixer-loader', 'less-loader'],fallback: 'vue-style-loader'})}}}
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ]
};