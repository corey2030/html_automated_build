class common{
    constructor(){
        this.init();
    }
    init(){
        this.browser();
    }
    browser(){
        this.device = 'pc';
        this.userAgent = navigator.userAgent;
        if(this.userAgent.indexOf('iPad') > -1){
            this.device = 'ipad';
        }
    }   
}

export default new common();